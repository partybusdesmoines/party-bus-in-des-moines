**Des Moines party bus**

In Des Moines, the party bus began back in 2006 and is one of the oldest and most experienced bus companies now running in the Des Moines area. 
Our party bus in Des Moines began business with one thing in mind, and it was people. 
While the main aim of most bus companies is to make money, ours has always been to help people have a fun and safe day.
That's why our motto, "For your safety, we're in the company, not the money" suits who we are.
Please Visit Our Website [Des Moines party bus](https://partybusdesmoines.com) for more information. 

---

## Our party bus in Des Moines services

Our Des Moines Party Bus is proud to have clean busses with some of the finest state-of-the-art sound systems, 
LED lighting, dance sticks and handrails for your comfort and enjoyment. 
I'm hiring the best drivers in the business. Most drivers are polite professionals at all times.
For all your hobbies, such as: bachelor's/parties, bachelor's birthday parties, corporate trips, holiday visits, 
football tailgates, small town pubs, and, of course, weddings, we're going to give you the best deals in town. 
We have some of the nicest busses in the business. Both busses feature state-of-the-art sound systems, LED lighting, handrails and dance poles.
We know it's never easy to pick a bus operator, but a group bus in Des Moines can make it simpler for you. 
We are a Des Moines-based company that covers the Ames metro, Cedar Rapids, Iowa City, and, of course, all over Des Moines.

